import pytest
from generator.generator import generate_task_info
from enums.global_enums import GlobalErrorMessages
from classes.tasks import Response


@pytest.mark.production
def test_create_task_with_real_data():
    """
    №1
    Тест-кейс: Создание задачи в существующем проекте

        Цель: Проверить, что API успешно возвращает информацию о создании задачи.
        Предусловие: Существует проект, и у вас есть действующий токен аутентификации.
        Шаги:
            Отправить запрос POST для URL https://api.clickup.com/api/v2/list/{list_id}/task,
            где list_id - идентификатор существующего списка.
        Ожидаемый результат:
            Ответ имеет статус код 200 OK.
            В ответе присутствуют необходимые поля: название задачи, описание и статус.
        Приоритет: Высокий
        Важность: Критически важно
    """

    # Инициализируем Response.
    post_task = Response()

    # Создаём новую таску.
    post_task.create_task()

    # Проверяем статус код, имя, описание и статус таски на соответствие.
    assert post_task.response_status == 200, GlobalErrorMessages.WRONG_STATUS_CODE
    assert post_task.response_json['name'] == post_task.name
    assert post_task.response_json['description'] == post_task.description
    assert post_task.response_json['status']['status'] == 'to do'


@pytest.mark.production
def test_create_task_with_not_real_data():
    """
    №2
    Тест-кейс: Попытка создания задачи в несуществующем списке существующего проекта

        Цель: Проверить, что API корректно обрабатывает запрос на создание задачи
              в несуществующей списке.
        Предусловие: Существует проект, и у вас есть действующий токен аутентификации.
        Шаги:
            Отправить запрос POST для URL https://api.clickup.com/api/v2/list/{list_id}/task,
            где list_id - идентификатор несуществующего списка.
        Ожидаемый результат:
            Ответ имеет статус код 401 Unauthorized.
            В ответе присутствуют необходимые поля: информация об ошибке.
        Приоритет: Высокий
        Важность: Критически важно
    """

    # Инициализируем Response.
    post_task = Response()

    # Генерируем list_id для таски.
    task_info = next(generate_task_info())

    # Создаём новую таску.
    post_task.create_task(list_id=task_info.list_id)

    # Проверяем статус код и json на соответствие.
    assert post_task.response_status == 401, GlobalErrorMessages.WRONG_STATUS_CODE
    assert post_task.response_json['err'] == 'Team not authorized', GlobalErrorMessages.WRONG_JSON


@pytest.mark.production
def test_get_existent_task():
    """
    №3
    Тест-кейс: Получение задачи существующего проекта

        Цель: Проверить, что API успешно возвращает информацию о существующей задаче.
        Предусловие: Существует проект с задачей, и у вас есть действующий токен аутентификации.
        Шаги:
            Отправить запрос GET для URL https://api.clickup.com/api/v2/task/{task_id}, где
            task_id - идентификатор существующей задачи.
        Ожидаемый результат:
            Ответ имеет статус код 200 OK.
            В ответе присутствуют необходимые поля: название задачи, описание и статус.
        Приоритет: Высокий
        Важность: Критически важно
    """

    # Инициализируем Response
    get_task = Response()

    # Получаем таску.
    get_task.get_task_by_id('86enk9a6c')

    # Проверяем статус код и id на соответствие.
    assert get_task.response_status == 200, GlobalErrorMessages.WRONG_STATUS_CODE
    assert get_task.response_json['id'] == get_task.task_id


@pytest.mark.production
def test_get_non_existent_task():
    """
       №4
       Тест-кейс: Обработка запроса на получение информации о несуществующей задаче

           Цель: Проверить, что API корректно обрабатывает запрос на получение информации
                 о несуществующей задаче.
           Предусловие: Существует проект без задачи, и у вас есть действующий токен аутентификации.
           Шаги:
               Отправить запрос GET для URL https://api.clickup.com/api/v2/task/{task_id}, где
               task_id - идентификатор несуществующей задачи.
           Ожидаемый результат:
               Ответ имеет статус код 401 Unauthorized.
               В ответе присутствуют необходимые поля: информация об ошибке.
           Приоритет: Высокий
           Важность: Критически важно
       """

    # Инициализируем Response
    get_task = Response()

    # Генерируем task_id
    task_info = next(generate_task_info())

    # Получаем таску.
    get_task.get_task_by_id(task_info.task_id)

    # Проверяем статус код и id на соответствие.
    assert get_task.response_status == 401, GlobalErrorMessages.WRONG_STATUS_CODE
    assert get_task.response_json['err'] == 'Team not authorized', GlobalErrorMessages.WRONG_JSON

