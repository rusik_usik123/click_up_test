import requests
from generator.generator import generate_task_info
from configs import AUTH, TEAM_ID, LIST_ID


class Response:

    def create_task(self, list_id=LIST_ID, team_id=TEAM_ID, auth=AUTH):
        """
        По умолчанию передаются существующие данные.
        Так же можно передать свои для разнообразия
        тестов.
        """

        # Генерируем имя и описание для таски.
        task_info = next(generate_task_info())

        # Сохраняем имя и описание в переменные для удобства.
        name = task_info.name
        description = task_info.description

        # Сохраняем в переменную энд поинт.
        response = "https://api.clickup.com/api/v2/list/" + list_id + "/task"

        # Передаём имя и описание таски.
        payload = {
            "name": name,
            "description": description,
            "markdown_description": description,
            'status': 'to do'
        }

        # Передаём Auth.
        headers = {
            "Content-Type": "application/json",
            "Authorization": auth
        }

        # Передаём team id.
        query = {
            "team_id": team_id
        }

        # Сохраняем данные для дальнейших операций с ними.
        self.response = requests.post(response, headers=headers, json=payload, params=query)
        self.response_status = self.response.status_code
        self.response_json = self.response.json()

        try:
            self.task_id = self.response_json['id']
        except:
            pass

        self.name = name
        self.description = description

    def get_task_by_id(self, id):
        self.task_id = id

        url = "https://api.clickup.com/api/v2/task/" + self.task_id

        query = {
            "custom_task_ids": "true",
            "team_id": TEAM_ID,
            "include_subtasks": "true",
            "include_markdown_description": "true"
        }

        headers = {"Authorization": AUTH}

        response = requests.get(url, headers=headers, params=query)
        self.response = response
        self.response_status = self.response.status_code
        self.response_json = self.response.json()
