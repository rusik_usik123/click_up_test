from data.data import Task
from faker import Faker
import random


faker_en = Faker('en_US')
Faker.seed()


def generate_task_info():
    yield Task(
        name=faker_en.first_name(),
        description=faker_en.job(),
        list_id=str(random.randint(1000000, 9999999)),
        task_id=str(random.randint(100000, 999999)),
    )

