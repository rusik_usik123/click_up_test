from dataclasses import dataclass


@dataclass
class Task:
    name: str = None
    description: str = None
    list_id: str = None
    task_id: str = None

