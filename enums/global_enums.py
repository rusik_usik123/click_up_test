from enum import Enum


class GlobalErrorMessages(Enum):
    WRONG_STATUS_CODE = "Ожидаемый и фактический статус код не совпадает"
    WRONG_JSON = 'Фактический и ожидаемый json не совпадают'
